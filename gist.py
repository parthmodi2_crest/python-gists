import os
import requests

class Gist(object):
    BASE_DIR = os.path.join("data", "gists")
    def __init__(self, raw_input):
        self.raw = raw_input
        
    @property
    def id(self):
        return self.raw.get('id')
    
    @property
    def url(self):
        return self.raw.get('url')
    
    @property
    def public(self):
        return self.raw.get('public')
    
    @property
    def html_url(self):
        return self.raw.get('html_url')
    
    @property
    def truncated(self):
        return self.raw.get('truncated')
    
    @property
    def comments(self):
        return self.raw.get('comments')
    
    def __repr__(self):
        return f"<Gist {self.id} {self.comments}>"
    
    @staticmethod
    def download_gist_content(filepath, gist_obj):
        """Download gist files in chunk"""
        for gist_fname, gist_fmeta in gist_obj["files"].items():
            r = requests.get(gist_fmeta["raw_url"], stream=True)
            with open(os.path.join(filepath, gist_fname), "wb") as f:
                for chunk in r.iter_content(1024):
                    if chunk:
                        f.write(chunk)
    
    @staticmethod
    def save_to_files(gist_objs):
        for gist_obj in gist_objs:
            base_dir = os.path.join(Gist.BASE_DIR, gist_obj["id"])
            if not os.path.exists(base_dir):
                os.makedirs(base_dir)
            Gist.download_gist_content(base_dir, gist_obj)