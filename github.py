import os
import logging
import requests
import time
from datetime import datetime
from requests.sessions import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from dotenv import load_dotenv

from gist import Gist

logger = logging.getLogger(__name__)
load_dotenv('.env')

class Github(object):
    BASE_URL = 'https://api.github.com'
    DEFAULT_MAX_RETRIES = 3
    GITHUB_TOKEN = os.getenv('GITHUB_TOKEN')
    API_RATE_LIMIT_TIME = 3000
    
    def __init__(self):
        self.default_headers = { "accept": 'application/vnd.github.v3+json', "Authorization": "token {}".format(self.GITHUB_TOKEN) }
        s = requests.Session()
        s.headers.update(self.default_headers)
        retries = Retry(total=self.DEFAULT_MAX_RETRIES, backoff_factor=1, allowed_methods=['GET', 'POST'])
        s.mount("https://", HTTPAdapter(max_retries=retries))
        self._connection = s
    
    def list_gists(self, per_page=None, max_pages=None):
        url = self.BASE_URL + "/gists"
        gists = []
        for page in self.fetch_records(url, params={"per_page": per_page}, max_pages=max_pages):
            for elem in page:
                gists.append(Gist(elem))
        return gists
    
    def list_public_gists(self, per_page=None, max_pages=None):
        url = self.BASE_URL + "/gists/public"
        
        gists = []
        for page in self.fetch_records(url, params={"per_page": per_page}, max_pages=max_pages):
            Gist.save_to_files(page)  
        return gists
    
    def fetch_records(self, url, params={}, max_pages=None):
        if max_pages is None: max_pages = 10
        pageCount = 0
        reqUrl = url
        
        while True:
            success, resp = self._make_rest_call(url=reqUrl, params=params)
            if success:
                curPage = resp.json()
                yield curPage
                nextPage = resp.links.get('next', {})
                pageCount += 1
                reqUrl = nextPage.get('url')
                
                if reqUrl is None or pageCount >= max_pages: break
            else:
                break

    def _make_rest_call(self, endpoint=None, url=None, method='get', headers=None, params=None, json=None, timeout=3):
        if url is None:
            url = "{}{}".format(self.BASE_URL, endpoint)
        
        if headers is None:
            req_headers = self.default_headers
        else:
            req_headers = self.default_headers.copy()
            req_headers.update(headers)
        
        response = [False, None]
        try:
            resp = self._connection.request(method=method, url=url, headers=req_headers, params=params, json=json, timeout=timeout)
            resp.raise_for_status()
            response = [True, resp]
        except requests.exceptions.HTTPError as he:
            if he.response.status_code == 429 or he.response.headers['X-RateLimit-Remaining'] == '0':
                limit_resets_at = datetime.fromtimestamp(int(he.response.headers['X-RateLimit-Reset']))
                sleep_time = (limit_resets_at - datetime.utcnow()).total_seconds()
                if sleep_time > self.API_RATE_LIMIT_TIME:
                    logger.error("Can not retry. Too long wait time.")
                else:
                    time.sleep(sleep_time)
                    logger.warning("Retrying again!")
                    self._make_rest_call(self, url=url, headers=req_headers, params=params, json=json)
            else:
                logger.error("{},{},{}".format(he.response.status_code, he.request.url, he.response.text))
        except requests.exceptions.ConnectionError as ce:
            logger.error(ce)
            logger.error("Could not connect, exhausted {} retries.".format(self.DEFAULT_MAX_RETRIES))    
        except requests.exceptions.RequestException as re:
            logger.error(re)
        
        return response
