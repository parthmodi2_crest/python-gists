from gist import Gist
from github import Github


def test_list_gists():
    g = Github()
    gists = g.list_gists(per_page=1, max_pages=1)
    
    assert len(gists) == 1
    for gist in gists:
        assert isinstance(gist, Gist)
